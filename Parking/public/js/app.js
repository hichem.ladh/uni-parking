//include express library
const express = require("express");
//refernce to express object in varible app 
const app = express();
var bodyParser = require('body-parser');
var routes = require("../../routes/routes");

// set view engine to egs
app.set("view engine", "ejs");
app.set("views", "../../views");


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', routes);
//app.use("/views", routes);

//include routes.js in to varible router


//app.use(express.static("/public"));
app.use(express.static('../../public'));



app.listen(8040, function () {
    console.log("Server Start in http://localhost:8040");
});




