const express = require("express");
const router = express.Router();
const parking = require("../models/parking.js");


router.get("/ReservierungListenseite", function (req, resp) {
    resp.render("ReservierungListenseite");
});

router.get("/AbonnementFormular", function (req, resp) {
    resp.render("AbonnementFormular.ejs");
});

router.get("/ReservierungDetailseite", function (req, resp) {
    resp.render("ReservierungDetailseite");
});

router.get("/ReservierungFormular", function (req, resp) {
    resp.render("ReservierungFormular");
});

router.get("/AbonnementDetailseite", function (req, resp) {
    resp.render("AbonnementDetailseite");
});

router.get("/AbonnementListenseite", function (req, resp) {
    resp.render("AbonnementListenseite", { List: parking.Abonnement.getListAbo() });
});

router.get("/", function (req, resp) {
    resp.render("ReservierungListenseite", { List: parking.Reservierung.getList() });
});

router.post("/Formula", function (req, resp) {
    console.log(req.body)
    parking.Reservierung.AddToList(req.body)
    resp.render("ReservierungListenseite", { List: parking.Reservierung.getList() });
});

module.exports = router;

router.post("/bezahlen", function (req, resp) {
    console.log(req.body)
    parking.Abonnement.AddToListAbo(req.body)
    resp.render("AbonnementListenseite", { List: parking.Abonnement.getListAbo() });
});





